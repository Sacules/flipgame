extends Sprite

export (int) var TYPE # TODO: Unificar haciendo que TYPE y SIDE estén
export (int) var SIDE # en un arreglo

var mouse_in = false
var fixed = true

onready var grid = get_parent()
onready var original_pos = Vector2()

var ficha_position = Vector2()

func _process(delta): # simplificar lógica para quitar ifs
	if mouse_in and not grid.dragging and fixed and Input.is_action_just_pressed("left_click"):
		start_dragging()

	if mouse_in and grid.dragging and not fixed and Input.is_action_pressed("left_click"):
		move_in_grid()

	if mouse_in and grid.dragging and not fixed and Input.is_action_just_released("left_click"):
		put_in_the_right_place()
		grid.check_connected()
		grid.connected = false


func start_dragging():
	# Saves position in case the cell you're moving towards is not empty
	original_pos = position
	grid.dragging = true
	fixed = false


func move_in_grid():
	# Como la posición de la ficha es relativa a la del tablero, 
	# hay que restarsela sino se desfasa bastante cada vez que
	# la queremos mover
	ficha_position = get_viewport().get_mouse_position() - grid.position

	# Mantiene la ficha dentro del tablero, los valores son relativos
	# a la posición del tablero
	# TODO: Agregar casos para que te deje mover la ficha solo horizontal y verticalmente
	position.x = clamp(ficha_position.x, 
					   grid.half_tile_size.x, 
					   grid.half_tile_size.x + grid.tile_size.x * (grid.grid_size.x - 1))

	position.y = clamp(ficha_position.y, 
					   grid.half_tile_size.y, 
					   grid.half_tile_size.y + grid.tile_size.y * (grid.grid_size.y - 1))


func put_in_the_right_place():
	# Hay que cuidar que si soltamos la ficha en la casilla donde estaba,
	# que simplemente vuelva ahí
	if grid.world_to_map(position) == grid.world_to_map(original_pos):
		position = original_pos
		print("returned successfully")

	else:
		if grid.is_cell_vacant(position):
			# Aseguramos que la ficha quede centrada en la nueva celda
			position = grid.world_to_map(position)
			position = grid.map_to_world(position) + grid.half_tile_size
			flip()
			grid.update_child_pos(self)
			prints("type", TYPE, "position:", position, "original pos:", original_pos, "fixed:", fixed, "dragging:", grid.dragging)
			original_pos = position

		else:
			position = original_pos
			print("no se superponen")

	grid.dragging = false
	fixed = true


func flip(): # TODO: Simplificar con match, mirar Tablero.spawn_ficha()
	if TYPE == 0 and SIDE == 0:
		texture = grid.type1_side2_tex
		SIDE = 1
	elif TYPE == 0 and SIDE == 1:
		texture = grid.type1_side1_tex
		SIDE = 0
	elif TYPE == 1 and SIDE == 0:
		texture = grid.type2_side2_tex
		SIDE = 1
	elif TYPE == 1 and SIDE == 1:
		texture = grid.type2_side1_tex
		SIDE = 0
	else:
		print("da fucc")


func _on_Area2D_mouse_entered():
	mouse_in = true


func _on_Area2D_mouse_exited():
	mouse_in = false