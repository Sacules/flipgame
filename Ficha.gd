extends Area2D

var TYPE
var SIDE

var grid

var mouse_in = false
var dragging = false


func _ready(): # Main trucho
	grid = get_parent()


func _process(delta):
	if (mouse_in and not dragging and Input.is_action_pressed("left_click")):
		original_pos = position
		prints("Original Position:", original_pos)
		dragging = true

	if (dragging and Input.is_action_pressed("left_click")):
		position = get_viewport().get_mouse_position()
		set_position(position)

	else:
		dragging = false


	







func _on_Ficha_mouse_entered():
	mouse_in = true


func _on_Ficha_mouse_exited():
	mouse_in = false
